rootProject.name = "adventofcode-2019"
include("day1")
include("day2")
include("day3")
include("day4")
