package services

import java.util.*

object SecureContainerService {
    fun countPossiblePasswords(minimumRange: Int, maxRange: Int): Int {
        var runningPassword = minimumRange

        var count = 0
        while (runningPassword <= maxRange) {
            if (!checkIfDigitsOnlyIncrease(runningPassword)) {
                runningPassword += 1
                continue
            }

            if (!checkIfDoubleDigit(runningPassword)) {
                runningPassword += 1
                continue
            }

            count += 1
            runningPassword += 1
        }

        return count
    }

    fun countPossiblePasswordsWithSingleMatchingPair(minimumRange: Int, maxRange: Int): Int {
        var runningPassword = minimumRange

        var count = 0
        while (runningPassword <= maxRange) {
            if (!checkIfDigitsOnlyIncrease(runningPassword)) {
                runningPassword += 1
                continue
            }

            if (!checkIfSinglePairExists(runningPassword)) {
                runningPassword += 1
                continue
            }

            count += 1
            runningPassword += 1
        }

        return count
    }

    private fun checkIfDigitsOnlyIncrease(password: Int): Boolean {
        require(password > 0) { "Password must be a positive value." }
        val stack = findIndividualDigits(password)

        var previousDigit = stack.pop()
        while (stack.isNotEmpty()) {
            val digit = stack.pop()

            if (previousDigit > digit) {
                return false
            }
            previousDigit = digit
        }
        return true
    }

    private fun checkIfDoubleDigit(password: Int): Boolean {
        require(password > 0) { "Password must be a positive value." }
        val stack = findIndividualDigits(password)

        var previousDigit = stack.pop()
        while (stack.isNotEmpty()) {
            val digit = stack.pop()

            if (previousDigit == digit) {
                return true
            }
            previousDigit = digit
        }
        return false
    }

    private fun checkIfSinglePairExists(password: Int): Boolean {
        require(password > 0) { "Password must be a positive value." }
        val stack = findIndividualDigits(password)

        var doubleDigitValue = -1
        var pairExists = false
        var previousDigit = stack.pop()
        while (stack.isNotEmpty()) {
            val digit = stack.pop()

            if (previousDigit != digit && pairExists)
                return true

            if (previousDigit == digit && digit == doubleDigitValue) {
                pairExists = false
            }
            else if (previousDigit == digit) {
                doubleDigitValue = digit
                pairExists = true
            }

            previousDigit = digit
        }

        return pairExists
    }

    private fun findIndividualDigits(value: Int): Stack<Int> {
        require(value > 0) { "Value must be a positive value." }
        val stack = Stack<Int>()

        var runningValue = value
        while (runningValue > 0) {
            stack.add(runningValue % 10)
            runningValue /= 10
        }

        return stack
    }
}
