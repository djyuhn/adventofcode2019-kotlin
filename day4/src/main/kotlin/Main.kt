import services.SecureContainerService

fun main() {
    val input: List<String> = {}::class.java.getResourceAsStream("day4.txt").bufferedReader().readLines()

    val range = input[0].split('-').map { it.toInt() }

    val possiblePasswords = SecureContainerService.countPossiblePasswords(range[0], range[1])
    println("Possible number of passwords: $possiblePasswords")

    val possiblePasswordsWithMatchingPair =
        SecureContainerService.countPossiblePasswordsWithSingleMatchingPair(range[0], range[1])

    println("Possible number of passwords with a pair of matching digits: $possiblePasswordsWithMatchingPair")
}
