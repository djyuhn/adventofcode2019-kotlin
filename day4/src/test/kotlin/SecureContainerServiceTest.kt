import services.SecureContainerService
import kotlin.test.Test
import kotlin.test.assertEquals

class SecureContainerServiceTest {
    @Test fun `countPossiblePasswordsWithSingleMatchingPair on value 112233 should return 1`() {
        val count = SecureContainerService.countPossiblePasswordsWithSingleMatchingPair(112233, 112233)

        val expectedResult = 1
        assertEquals(expectedResult, count)
    }

    @Test fun `countPossiblePasswordsWithSingleMatchingPair on value 123444 should return 0`() {
        val count = SecureContainerService.countPossiblePasswordsWithSingleMatchingPair(123444, 123444)

        val expectedResult = 0
        assertEquals(expectedResult, count)
    }

    @Test fun `countPossiblePasswordsWithSingleMatchingPair on value 111122 should return 1`() {
        val count = SecureContainerService.countPossiblePasswordsWithSingleMatchingPair(111122, 111122)

        val expectedResult = 1
        assertEquals(expectedResult, count)
    }

    @Test fun `countPossiblePasswordsWithSingleMatchingPair on value 122333 should return 1`() {
        val count = SecureContainerService.countPossiblePasswordsWithSingleMatchingPair(122333, 122333)

        val expectedResult = 1
        assertEquals(expectedResult, count)
    }
}
