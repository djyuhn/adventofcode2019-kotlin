import services.IntcodeProgram

const val NOUN_MAX = 99
const val VERB_MAX = 99
const val DESIRED_OUTPUT = 19690720
const val POSITION_ONE = 12
const val POSITION_TWO = 2

fun main() {
    // Replace positions 1 and 2 as specified in problem of part 1
    val input: List<Int> = {}::class.java.getResourceAsStream("day2.txt").bufferedReader().readLines()
            .map { line -> line.split(",") }
            .flatten()
            .mapIndexed() { index, input ->
                when (index) {
                    1 -> POSITION_ONE
                    2 -> POSITION_TWO
                    else -> input.toInt()
                }
            }

    val processedCode = IntcodeProgram.performIntCode(input)
    println("Processed value is: ${processedCode[0]}")

    val foundOperation = IntcodeProgram.findNounAndVerb(NOUN_MAX, VERB_MAX, input, DESIRED_OUTPUT)

    println("Noun value is: ${foundOperation?.firstInputPosition}")
    println("Verb value is: ${foundOperation?.secondInputPosition}")
}