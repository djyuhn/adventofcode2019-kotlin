package services

import models.IOperation
import models.Opcode
import models.Operation

object IntcodeProgram : IIntcodeProgram {

    /**
     * Performs the Intcode [input] by following the rules dictated by [IOperation] that are created during the
     * processing.
     *
     */
    override fun performIntCode(input: List<Int>): List<Int> {
        var index: Int = 0
        var inputList = input
        while (!isHaltOperation(input[index])) {
            val operation: IOperation = translateToOperation(input, index)
            index += 4
            inputList = performOperation(operation, inputList)
        }

        return inputList
    }

    /**
     * Determines the noun and verb of an [input] through iterating up to the [nounMax] and [verbMax] that results in
     * the [desiredOutput] of the Intcode. Will return the [IOperation] that contains the noun and verb.
     */
    override fun findNounAndVerb(nounMax: Int, verbMax: Int, input: Collection<Int>, desiredOutput: Int): IOperation? {

        for (noun in 0..nounMax) {
            for (verb in 0..verbMax) {
                val inputCopy: List<Int> = input.toList().mapIndexed { index, intCode ->
                    when (index) {
                        1 -> {
                            noun
                        }
                        2 -> {
                            verb
                        }
                        else -> intCode
                    }
                }

                val outputList: List<Int> = performIntCode(inputCopy)

                if (outputList[0] == desiredOutput) {
                    return Operation(Opcode.getByValue(inputCopy[0]), noun, verb, inputCopy[3])
                }
            }
        }

        return null
    }

    private fun isHaltOperation(position: Int): Boolean {
        return position == Opcode.Halt.value
    }

    private fun translateToOperation(input: List<Int>, position: Int): IOperation {
        val opCode: Opcode = Opcode.getByValue(input[position])

        return Operation(opCode, input[position + 1], input[position + 2], input[position + 3])
    }

    private fun performOperation(operation: IOperation, input: List<Int>): List<Int> {

        try {
            when (operation.opcode) {
                Opcode.Addition -> {
                    val sum = input[operation.firstInputPosition] + input[operation.secondInputPosition]
                    return input.mapIndexed { index, value ->
                        if (index == operation.outputPosition) sum else value
                    }
                }
                Opcode.Multiplication -> {
                    val product = input[operation.firstInputPosition] * input[operation.secondInputPosition];
                    return input.mapIndexed { index, value ->
                        if (index == operation.outputPosition) product else value
                    }
                }
                Opcode.Halt -> return input

                else -> throw IllegalArgumentException("Operation opcode of ${operation.opcode} is invalid.")
            }
        } catch (exception: IndexOutOfBoundsException) {
            println(exception.message)
            throw exception
        }
    }
}