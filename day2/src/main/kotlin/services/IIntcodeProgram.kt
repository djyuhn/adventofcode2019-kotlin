package services

import models.IOperation

interface IIntcodeProgram {
    fun performIntCode(input: List<Int>): List<Int>
    fun findNounAndVerb(nounMax: Int, verbMax: Int, input: Collection<Int>, desiredOutput: Int): IOperation?
}