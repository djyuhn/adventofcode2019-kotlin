package models

enum class Opcode(val value: Int) {
    Addition(1),
    Multiplication(2),
    Halt(99);

    companion object {
        private val values = values()
        fun getByValue(value: Int) = values.first { it.value == value }
    }
}