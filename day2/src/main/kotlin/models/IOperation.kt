package models

interface IOperation {
    val opcode: Opcode
    val firstInputPosition: Int
    val secondInputPosition: Int
    val outputPosition: Int
}