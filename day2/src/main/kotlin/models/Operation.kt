package models

data class Operation(override val opcode: Opcode, override val firstInputPosition: Int,
                     override val secondInputPosition: Int, override val outputPosition: Int) : IOperation