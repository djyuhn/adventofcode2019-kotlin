object FuelCalculator : IFuelCalculator {
    /**
     * Calculates and returns the fuel for the given [moduleWeights].
     */
    override fun calculateFuel(moduleWeights: List<Int>): Int {
        val fuelSum: Int = moduleWeights.sumBy { moduleWeight: Int -> moduleWeight / 3 - 2 }
        return fuelSum
    }

    /**
     * Calculates and returns the corrected fuel for the given [moduleWeights]
     *
     * Like [calculateFuel], finds the sum of the fuel necessary for the [moduleWeights]. Also finds the fuel needed for
     * the fuel itself.
     */
    override fun calculateCorrectedFuel(moduleWeights: List<Int>): Int {
        val fuelSum: Int = moduleWeights.sumBy { moduleWeight: Int -> fuelCalculation(moduleWeight) }
        return fuelSum
    }

    private fun fuelCalculation(weight: Int): Int {
        val fuel = weight / 3 - 2

        if (fuel <= 0) {
            return 0
        }

        return fuel + fuelCalculation(fuel)
    }
}