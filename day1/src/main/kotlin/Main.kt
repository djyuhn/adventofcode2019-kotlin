fun main() {
    val input: List<Int> = {}::class.java.getResourceAsStream("day1.txt").bufferedReader().readLines().map { it.toInt() }

    val fuelCalculator: IFuelCalculator = FuelCalculator

    val fuel: Int = fuelCalculator.calculateFuel(input)
    val correctedFuel: Int = fuelCalculator.calculateCorrectedFuel(input)

    println(fuel)
    println(correctedFuel)
}