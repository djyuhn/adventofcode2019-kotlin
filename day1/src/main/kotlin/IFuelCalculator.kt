interface IFuelCalculator {
    /**
     * Calculates and returns the fuel for the given [moduleWeights].
     */
    fun calculateFuel(moduleWeights: List<Int>): Int

    /**
     * Calculates and returns the corrected fuel for the given [moduleWeights]
     *
     * Like [calculateFuel], finds the sum of the fuel necessary for the [moduleWeights]. Also finds the fuel needed for
     * the fuel itself.
     */
    fun calculateCorrectedFuel(moduleWeights: List<Int>): Int

}