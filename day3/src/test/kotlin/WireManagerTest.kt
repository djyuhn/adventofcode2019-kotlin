import services.IWireCreator
import services.WireCreator
import services.WireManager
import kotlin.test.Test
import kotlin.test.assertEquals

class WireManagerTest {
    @Test fun `test case 1 should return 159`() {
        val input: List<String> = {}::class.java.getResourceAsStream("testCase1.txt").bufferedReader().readLines()

        val wireCreator: IWireCreator = WireCreator

        val wireOneRaw = input[0].split(',')
        val wireOneMovement = wireCreator.createWire(wireOneRaw, 0)

        val wireTwoRaw = input[1].split(',')
        val wireTwoMovement = wireCreator.createWire(wireTwoRaw, 1)

        val wireManager = WireManager

        val distance = wireManager.findNearestIntersectionDistance(wireOneMovement, wireTwoMovement)

        val expectedResult = 159
        assertEquals(expectedResult, distance)
    }

    @Test fun `test case 2 should return 135`() {
        val input: List<String> = {}::class.java.getResourceAsStream("testCase2.txt").bufferedReader().readLines()

        val wireCreator: IWireCreator = WireCreator

        val wireOneRaw = input[0].split(',')
        val wireOneMovement = wireCreator.createWire(wireOneRaw, 0)

        val wireTwoRaw = input[1].split(',')
        val wireTwoMovement = wireCreator.createWire(wireTwoRaw, 1)

        val wireManager = WireManager

        val distance = wireManager.findNearestIntersectionDistance(wireOneMovement, wireTwoMovement)

        val expectedResult = 135
        assertEquals(expectedResult, distance)
    }
}
