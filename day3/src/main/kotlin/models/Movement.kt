package models

data class Movement(val direction: Direction, val distance: Int, val wireNumber: Int)
