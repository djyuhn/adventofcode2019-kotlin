package models

data class WireSegment(val lineSegment: LineSegment, val wireNumber: Int)

