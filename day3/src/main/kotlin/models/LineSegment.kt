package models

import kotlin.math.max
import kotlin.math.min

data class LineSegment(val pointA: Point, val pointB: Point) {
    val xCoordMin = min(pointA.xValue, pointB.xValue)
    val xCoordMax = max(pointA.xValue, pointB.xValue)

    val slope = (pointA.yValue - pointB.yValue) / (pointA.xValue - pointB.xValue).toDouble()

    val yIntercept = pointA.yValue - (pointA.xValue * slope)
    val xIntercept =
        if (yIntercept == Double.POSITIVE_INFINITY || yIntercept == Double.NEGATIVE_INFINITY)
            pointA.xValue.toDouble()
        else {
            if (yIntercept == 0.0)
                pointA.xValue.toDouble()
            else (yIntercept * -1 / slope)
        }
}
