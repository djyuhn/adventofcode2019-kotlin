package models

data class Point(val xValue: Int, val yValue: Int)
