package models

enum class LineDirection {
    Clockwise,
    CounterClockwise,
    Collinear
}
