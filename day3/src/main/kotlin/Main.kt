import services.IWireCreator
import services.WireCreator
import services.WireManager

fun main() {
    val input: List<String> = {}::class.java.getResourceAsStream("day3.txt").bufferedReader().readLines()

    val wireCreator: IWireCreator = WireCreator

    val wireOneRaw = input[0].split(',')
    val wireOneMovement = wireCreator.createWire(wireOneRaw, 0)

    val wireTwoRaw = input[1].split(',')
    val wireTwoMovement = wireCreator.createWire(wireTwoRaw, 1)

    val wireManager = WireManager

    val distance = wireManager.findNearestIntersectionDistance(wireOneMovement, wireTwoMovement)

    println(distance)

}
