package services

import models.Direction
import models.Movement

object WireCreator : IWireCreator {
    override fun createWire(rawWire: List<String>, wireNumber: Int): List<Movement> {

        return rawWire.map { text ->
            when (text[0]) {
                'U' -> {
                    Movement(Direction.Up, text.substring(1).toInt(), wireNumber)
                }
                'D' -> {
                    Movement(Direction.Down, text.substring(1).toInt(), wireNumber)
                }
                'L' -> {
                    Movement(Direction.Left, text.substring(1).toInt(), wireNumber)
                }
                'R' -> {
                    Movement(Direction.Right, text.substring(1).toInt(), wireNumber)
                }
                else -> throw IllegalArgumentException("String format of $text is invalid.")
            }
        }
    }
}
