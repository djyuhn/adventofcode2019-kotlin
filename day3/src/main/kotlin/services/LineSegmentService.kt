package services

import models.LineDirection
import models.LineSegment
import models.Point
import kotlin.math.max
import kotlin.math.min

object LineSegmentService {

    /**
     * Checks if [segmentOne] and [segmentTwo] intersect.
     * [Resource](https://www.tutorialspoint.com/Check-if-two-line-segments-intersect)
     */
    fun checkForIntersection(segmentOne: LineSegment, segmentTwo: LineSegment): Boolean {

        val directionOne = determineDirection(segmentOne.pointA, segmentOne.pointB, segmentTwo.pointA)
        val directionTwo = determineDirection(segmentOne.pointA, segmentOne.pointB, segmentTwo.pointB)
        val directionThree = determineDirection(segmentTwo.pointA, segmentTwo.pointB, segmentOne.pointA)
        val directionFour = determineDirection(segmentTwo.pointA, segmentTwo.pointB, segmentOne.pointB)

        if (directionOne != directionTwo && directionThree != directionFour)
            return true

        if (directionOne == LineDirection.Collinear && isPointOnSegment(segmentOne, segmentTwo.pointA))
            return true

        if (directionTwo == LineDirection.Collinear && isPointOnSegment(segmentOne, segmentTwo.pointB))
            return true

        if (directionThree == LineDirection.Collinear && isPointOnSegment(segmentTwo, segmentOne.pointA))
            return true

        if (directionFour == LineDirection.Collinear && isPointOnSegment(segmentTwo, segmentOne.pointB))
            return true

        return false
    }

    /**
     * Determines the directionality of 3 given points.
     * [Resource](https://www.tutorialspoint.com/Check-if-two-line-segments-intersect)
     */
    fun determineDirection(pointA: Point, pointB: Point, pointC: Point): LineDirection {
        val pointDirection =
            (pointB.yValue - pointA.yValue) * (pointC.xValue - pointB.xValue) - (pointB.xValue - pointA.xValue) * (pointC.yValue - pointB.yValue)

        if (pointDirection == 0)
            return LineDirection.Collinear
        if (pointDirection < 0)
            return LineDirection.CounterClockwise

        return LineDirection.Clockwise
    }

    /**
     * Determines if a given [point] lies on the given [lineSegment]
     */
    fun isPointOnSegment(lineSegment: LineSegment, point: Point): Boolean {
        if (point.xValue <= lineSegment.pointA.xValue
            && point.xValue <= lineSegment.pointB.xValue
            && point.yValue <= max(lineSegment.pointA.yValue, lineSegment.pointB.yValue)
            && point.yValue >= min(lineSegment.pointA.yValue, lineSegment.pointB.yValue)
        ) {
            return true
        }

        return false
    }

    /**
     * Calculates the intersection of [lineSegmentOne] and [lineSegmentTwo] and returns a [Point].
     */
    fun calculateIntersection(lineSegmentOne: LineSegment, lineSegmentTwo: LineSegment): Point? {

        if (checkForIntersection(lineSegmentOne, lineSegmentTwo)) {

            // If Segment 1 is horizontal or Segment 2 is vertical
            if (lineSegmentOne.xIntercept.isInfinite() || lineSegmentTwo.yIntercept.isInfinite()) {
                return Point(lineSegmentTwo.xIntercept.toInt(), lineSegmentOne.yIntercept.toInt())
            }

            // If Segment 1 is vertical or Segment 2 is horizontal
            if (lineSegmentTwo.xIntercept.isInfinite() || lineSegmentOne.yIntercept.isInfinite()) {
                return Point(lineSegmentOne.xIntercept.toInt(), lineSegmentTwo.yIntercept.toInt())
            }

            // If both segments have the same x-intercept
            if (lineSegmentOne.xIntercept == lineSegmentTwo.xIntercept) {
                if (lineSegmentOne.pointA.xValue == lineSegmentTwo.pointA.xValue || lineSegmentOne.pointA.xValue == lineSegmentTwo.pointB.xValue) {
                    return Point(lineSegmentOne.pointA.xValue, lineSegmentOne.yIntercept.toInt())
                }
                if (lineSegmentOne.pointB.xValue == lineSegmentTwo.pointA.xValue || lineSegmentOne.pointB.xValue == lineSegmentTwo.pointB.xValue) {
                    return Point(lineSegmentOne.pointB.xValue, lineSegmentOne.yIntercept.toInt())
                }
            }

            // If both segments are have the same y-intercept
            if (lineSegmentOne.yIntercept == lineSegmentTwo.yIntercept) {
                if (lineSegmentOne.pointA.yValue == lineSegmentTwo.pointA.yValue || lineSegmentOne.pointA.yValue == lineSegmentTwo.pointB.yValue) {
                    return Point(lineSegmentOne.pointA.xValue, lineSegmentOne.yIntercept.toInt())
                }
                if (lineSegmentOne.pointB.yValue == lineSegmentTwo.pointA.yValue || lineSegmentOne.pointB.yValue == lineSegmentTwo.pointB.yValue) {
                    return Point(lineSegmentOne.pointB.xValue, lineSegmentOne.yIntercept.toInt())
                }
            }

            val xCoord =
                (lineSegmentTwo.yIntercept - lineSegmentOne.yIntercept) / (lineSegmentOne.slope - lineSegmentTwo.slope)
            val yCoord = lineSegmentOne.slope * xCoord + lineSegmentOne.yIntercept

            return Point(xCoord.toInt(), yCoord.toInt())
        }

        return null
    }
}
