package services

import models.Movement

interface IWireManager {
    /**
     * From a common origin, finds the distance of the nearest intersection of [wireOne] and [wireTwo]
     */
    fun findNearestIntersectionDistance(wireOne: List<Movement>, wireTwo: List<Movement>): Int?
}
