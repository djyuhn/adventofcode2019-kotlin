package services

import models.*
import kotlin.math.abs

object WireManager : IWireManager {

    /**
     * From a common origin, finds the distance of the nearest intersection of [wireOne] and [wireTwo]
     */
    override fun findNearestIntersectionDistance(wireOne: List<Movement>, wireTwo: List<Movement>): Int? {
        val origin = Point(0, 0)

        val wireOneSegments: List<WireSegment> = createWireSegments(wireOne, origin)
        val wireTwoSegments: List<WireSegment> = createWireSegments(wireTwo, origin)
        val intersections = findIntersections(wireOneSegments, wireTwoSegments)

        return calculateNearestIntersectionDistance(intersections, origin)
    }

    private fun calculateNearestIntersectionDistance(intersections: Set<Point>, origin: Point): Int? {
        val intersectionsWithoutOrigin = intersections.filter { intersection -> intersection != origin }
        return intersectionsWithoutOrigin.map { intersection ->
                abs(origin.xValue - intersection.xValue) + abs(origin.yValue - intersection.yValue)
            }.min()

    }

    /**
     * Using the [currentPosition], will return the new position after the [movement] has occurred.
     */
    private fun movementTracker(movement: Movement, currentPosition: Point): Point {
        when (movement.direction) {
            Direction.Up -> {
                val movementUp = currentPosition.yValue + movement.distance
                return Point(currentPosition.xValue, movementUp)
            }
            Direction.Down -> {
                val movementDown = currentPosition.yValue - movement.distance
                return Point(currentPosition.xValue, movementDown)
            }
            Direction.Left -> {
                val movementLeft = currentPosition.xValue - movement.distance
                return Point(movementLeft, currentPosition.yValue)
            }
            Direction.Right -> {
                val movementRight = currentPosition.xValue + movement.distance
                return Point(movementRight, currentPosition.yValue)
            }
        }
    }

    /**
     * Given a [wire] and a [startPosition], will return a [WireSegment] list using the movements of the provided wire
     */
    private fun createWireSegments(wire: List<Movement>, startPosition: Point): List<WireSegment> {
        var currentPosition = startPosition.copy()
        val wireSegments: MutableList<WireSegment> = mutableListOf()

        wire.forEach { movement ->
            val endPosition = movementTracker(movement, currentPosition)

            val lineSegment = LineSegment(currentPosition, endPosition)
            wireSegments.add(WireSegment(lineSegment, movement.wireNumber))

            currentPosition = endPosition
        }

        return wireSegments
    }

    /**
     * Returns a set of [Point] indicating the intersections of [wireOne] and [wireTwo]
     */
    private fun findIntersections(wireOne: List<WireSegment>, wireTwo: List<WireSegment>): Set<Point> {

        val wireSegments = wireOne.plus(wireTwo).sortedBy { it.lineSegment.xCoordMin }
        val intersections: MutableList<Point> = mutableListOf()
        var activeSegments: MutableList<WireSegment> = mutableListOf()
        var xCoord: Int

        wireSegments.forEach { wireSegment ->
            activeSegments.forEach { segment ->
                if (wireSegment.wireNumber != segment.wireNumber) {
                    val intersection =
                        LineSegmentService.calculateIntersection(wireSegment.lineSegment, segment.lineSegment)
                    intersection?.let { intersections.add(it) }
                }
            }
            xCoord = wireSegment.lineSegment.xCoordMin
            activeSegments =
                activeSegments.filter { activeSegment -> activeSegment.lineSegment.xCoordMax >= xCoord }
                    .toMutableList()
            activeSegments.add(wireSegment)
        }

        return intersections.toSet()

    }

}
