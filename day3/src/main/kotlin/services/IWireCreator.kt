package services

import models.Movement

interface IWireCreator {
    fun createWire(rawWire: List<String>, wireNumber: Int): List<Movement>
}
